// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   wstr_utf8_converter.cpp                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: wide-aze <wide-aze@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/23 12:03:45 by wide-aze          #+#    #+#             //
//   Updated: 2015/11/24 10:31:56 by wide-aze         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

/*
** c++ functions to convert wstr <-> utf8
** return: return the converted str or an empty string if bad byte found
**
** /!\ use init function to set locale before do anything /!\
*/

#include <clocale>
#include <string>
#include <iostream>
using namespace std;

void wstr_utf8_converter_init(void){locale::global(locale(""));}

wstring		utf8_to_wstr(const string& src)
{
	wstring			wstr;
	wchar_t			wc = 0;
	int				bytes = 0;
	unsigned char	c;

	for (size_t i = 0; i < src.size(); i++)
	{
		c = (unsigned char)src[i];
		if (c <= 0x7f) //first byte
		{
			if (bytes)
				return (wstring());
			wstr.push_back((wchar_t)c);
    		}
    		else if (c <= 0xbf) //second/third/etc byte
    		{
			if (bytes)
      			{
        		wc = ((wc << 6) | (c & 0x3f));
        		bytes--;
        		if (!bytes)
				wstr.push_back(wc);
			}
			else
				return (wstring());
    		}
		else if (c <= 0xdf) //second byte
		{
			bytes = 1;
			wc = c & 0x1f;
		}
		else if (c <= 0xef) //third byte
		{
			bytes = 2;
			wc = c & 0x0f;
		}
		else if (c <= 0xf7) //fourth byte
		{
			bytes = 3;
			wc = c & 0x07;
		}
		else
			return (wstring());
	}
	if (bytes)
		return (wstring());
	return (wstr);
}

string		wstr_to_utf8(const wstring &src)
{
	string		str;
	wchar_t 	w;

	for (size_t i = 0; i < src.size(); i++)
	{
		w = src[i];
		if (w <= 0x7f)
			str.push_back((char)w);
		else if (w <= 0x7ff)
		{
			str.push_back(0xc0 | ((w >> 6) & 0x1f));
			str.push_back(0x80 | (w & 0x3f));
		}
		else if (w <= 0xffff)
		{
			str.push_back(0xe0 | ((w >> 12) & 0x0f));
			str.push_back(0x80 | ((w >> 6) & 0x3f));
			str.push_back(0x80 | (w & 0x3f));
		}
		else if (w <= 0x10ffff)
		{
			str.push_back(0xf0 | ((w >> 18) & 0x07));
			str.push_back(0x80 | ((w >> 12) & 0x3f));
			str.push_back(0x80 | ((w >> 6) & 0x3f));
			str.push_back(0x80 | (w & 0x3f));
		}
		else
			return (string());
	}
	return (str);
}
